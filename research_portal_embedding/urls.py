from django.urls import path

from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('default.css', views.default_css, name="default_css"),
]
