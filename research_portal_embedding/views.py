import os.path as osp

from django.shortcuts import get_object_or_404 # render
from django.http import HttpResponse, Http404

from .models import Research_portal_list

# testurl
# http://127.0.0.1:8000/research_portal_embedding/?id=1

current_dir = osp.dirname(__file__)

def index(request):
    if "id" in request.GET:
        identity = int(request.GET["id"][0])
        rpl = get_object_or_404(Research_portal_list.objects, pk=identity)
    else:
        raise Http404("Please use id to get page")
    return HttpResponse(rpl.parsed_html)


def default_css(request):
    css_path = osp.join(current_dir, "static/default_research_portal.css")
    with open(css_path, 'r') as f:
        css_str = f.read()
    return HttpResponse(css_str)

