import argparse
import os
import os.path as osp
import json
import sys

from .load_and_parse_html import load_and_parse_html


def find_django_site_name(base_dir):
    """ Finding the right django site name to use by finding a settings file """
    for root, dirs, files in os.walk(base_dir, topdown=False):
        for name in files:
            if name == "settings.py":
                return osp.basename(root)
    raise ValueError("Django settings file not found")


def main():
    parser = argparse.ArgumentParser(
        description="Run with ´python -m research_portal_embedding ...´"
    )
    parser.add_argument(
        "--update-database",
        "-ud",
        action="store_true",
        help="Update all entries in the database, will override any other arguments.",
    )
    parser.add_argument(
        "--config",
        "-c",
        help="""
        Config file that can give similar information as below in json format.
        Some arguments can only be provided through the config file such as extra_css_file and more
        See docs of load_and_parse_html for more information. Each parameter to the load_and_parse_html function can be a key in the config file.
        """,
    )
    parser.add_argument(
        "--research-portal-link",
        "-l",
        help="Link to the research portal page with a list to embed. Uses the default behavior of the manipulation. To get more options use a json config file.",
    )
    parser.add_argument("--output-file", "-o", help="File to save results in.")
    args = parser.parse_args()

    if len(sys.argv) == 1:
        parser.print_help()
        exit()

    if args.update_database:
        import django

        site_name = find_django_site_name(".")
        os.environ.setdefault("DJANGO_SETTINGS_MODULE", "{}.settings".format(site_name))
        try:
            django.setup()
            from .models import Research_portal_list
        except ModuleNotFoundError:
            raise ModuleNotFoundError(
                "Run script with command shown in help description"
            )

        if args.update_database:
            all_lists = Research_portal_list.objects.all()
            for l in all_lists:
                print("Updating: {}".format(str(l)))
                l.save()
    else:
        config = {}

        if args.config:
            if osp.isfile(args.config):
                with open(args.config, "r") as f:
                    config.update(json.load(f))
            else:
                raise ValueError("Config file not found")

        if not args.research_portal_link is None:
            config["research_portal_link"] = args.research_portal_link

        if config["research_portal_link"] is None:
            raise ValueError(
                "Research portal link must be given either in json config or as an explicit argument"
            )
        if args.output_file is None:
            raise ValueError("Output file must be given")

        parsed_html = load_and_parse_html(**config)
        with open(args.output_file, "w") as f:
            f.write(parsed_html)


if __name__ == "__main__":
    main()
