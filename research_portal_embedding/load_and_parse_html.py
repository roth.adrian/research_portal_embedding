import bs4
import urllib.request
import re

from . import special_processing

default_list_searcher = {"id": "main-content"}
default_base_web_url = "https://portal.research.lu.se"


def load_and_parse_html(
    research_portal_link,
    extra_html="",
    extra_beautiful=[],
    remove_empty_tags=True,
    list_searcher=default_list_searcher,
    base_web_url=default_base_web_url,
    embed_css=False,
):
    """
    Take input research portal link and try to convert the html file to a neat list to the output file by doing some processing along the way.
    If you want inspiration of how the bottom parameters can look like, see examples above.

    research_portal_link: str
        http link to a research portal page that after a search has some list to be used.
    extra_html: str
        A string of html to insert at the start of the output file
    extra_beautiful: list of lists of exactly 2 dicts
        To give the list that little extra to make it perfect it is possible to do some post proccesing of the html before printing.
        Each sublist does some kind of work where the first dict in this list is kwargs for a BeautifulSoup search query find_all (https://www.crummy.com/software/BeautifulSoup/bs4/doc/#find-all). Here if the value for any key as string starts with "_re.", the rest of the string will be compiled using regex rules.
        The second dict has information of what to to with the found tags depending on the key names. The following happens with each key and in the following order if all keys are present:
            "filter": bool this is special since it loops through all the list items and checks the search criteria for each. If the bool value is true the items where the search terms are found is kept, if false they are removed.
            "move_tag": list of strings, gives the opportunity to move to a different tag before doing any other work below. The list can either consist of the string "parent" to go to parent tag, "<tagname>" (example "img") to go to the image of the tag or an integer which is the index of the child to go to. If the movement is not possible it will not do anything with the found tag
                https://www.crummy.com/software/BeautifulSoup/bs4/doc/#unwrap
            "emptify": bool to say if the tags should be emptied of its contents
            "prepend_html": string value html is prepended to the tags contents
            "append_html": same as previous but appended instead
            "set_attributes": dict with name value attributes to set to the tags, if the value of the key attribute is None (null), the attribute will be removed.
            "decompose": bool that if true will remove the tags and do not unwrap results.
            "unwrap": bool that if true will unwrap the tags.
            "special": kwargs for special function. Some functions have been written in module special_processing to do some slightly more complex tasks. Here the first search_identifier is only the function name as a string. See the module for what is implemented.
    remove_empty_tags: bool
        after the extra_beautiful work go through the html and remove all tags that have empty content.
    list_searcher: dict
        same as searchers in extra beautiful, kwargs given to find_all to get the list that will be used for the remaining work.
    base_web_url: string
        if another page than portal.research.lu.se is used this url will be used to fix image urls from relative to absolute.
    embed_css: self explanatory right!?

    returns:
        string of the html page
    """
    with urllib.request.urlopen(research_portal_link) as f:
        html = f.read().decode("utf-8")
    html = html.replace("\\n", "")

    publication_soup = bs4.BeautifulSoup(html, "html.parser")

    # This is the class name assumed to be the list for everything on the research portal
    # search_class = "search-result-container"
    list_div = publication_soup.find_all(**list_searcher)
    if len(list_div) == 0:
        raise ValueError("No search result list found in the given link")
    elif len(list_div) > 1:
        print(
            "Warning: found unusually many tags with the list class, something might be wrong."
        )
    list_div = list_div[0]

    # adding possible extra html code
    if extra_html is not None:
        extra_soup = bs4.BeautifulSoup(extra_html, "html.parser")
        for tag in extra_soup.contents[::-1]:
            list_div.insert(0, tag)

    # Complement all links with aria labels
    for link in list_div.find_all("a"):
        if "aria-label" not in link:
            new_text = "{} - portal.research.lu.se".format(link.string)
            link["aria-label"] = new_text

    # fixing image urls
    for img in list_div.find_all("img", class_="image"):
        if not img["src"].startswith("http"):
            img["src"] = "{}{}".format(base_web_url, img["src"])

    # Doing the possible extra work to make it beautiful
    for search_identifier, work in extra_beautiful:
        try:
            if "special" in work:
                function_name = search_identifier
                if hasattr(special_processing, function_name):
                    getattr(special_processing, function_name)(
                        publication_soup, list_div, **work["special"]
                    )
                else:
                    raise ValueError(
                        "No special function {} found".format(function_name)
                    )
                continue

            if "class" in search_identifier:
                search_identifier["class_"] = search_identifier["class"]
                del search_identifier["class"]

            for key in search_identifier.keys():
                val = search_identifier[key]
                if isinstance(val, str) and val.startswith("_re."):
                    search_identifier[key] = re.compile(val[3:])

            if "filter" in work:
                items = list_div.find_all(name="li")
                for item in items:
                    isin = item.find(**search_identifier) is not None
                    if (not isin and work["filter"]) or (isin and not work["filter"]):
                        item.decompose()
                continue

            found_tags = list_div.find_all(**search_identifier)
            if len(found_tags) == 0:
                raise ValueError(
                    "No tags found for search {}".format(search_identifier)
                )
            for tag in found_tags:
                if "move_tag" in work:
                    movements = work["move_tag"]
                    try:
                        for move in movements:
                            if isinstance(move, str):
                                tag = getattr(tag, move)
                            elif isinstance(move, int):
                                tag = tag.contents[move]
                    except AttributeError:
                        print("Error in move_tag")
                        continue
                if "emptify" in work and work["emptify"]:
                    tag.string = ""
                if "prepend_html" in work:
                    soup = bs4.BeautifulSoup(work["prepend_html"], "html.parser")
                    tag.insert(0, soup)
                if "append_html" in work:
                    soup = bs4.BeautifulSoup(work["append_html"], "html.parser")
                    tag.append(soup)
                if "set_attributes" in work:
                    for key, value in work["set_attributes"].items():
                        if value is None:
                            del tag[key]
                            continue
                        if key == "string":
                            tag.string = value
                        else:
                            tag[key] = value
                if "decompose" in work and work["decompose"]:
                    tag.decompose()
                    continue
                if "unwrap" in work and work["unwrap"]:
                    tag.unwrap()
                    continue
        except Exception as e:
            print(f"Error on extra beautiful, {e}")

    if remove_empty_tags:

        def empty_string(tag):
            """Check if tag is empty in the sense that it has no content, might be more exceptions than the img tag that should be added in the first if statement"""
            if (
                isinstance(tag, bs4.NavigableString)
                or tag.name == "img"
                or tag.name == "link"
                or tag.name == "i"
            ):
                return False
            if tag.string is None and len(tag.contents) == 0:
                return True
            return False

        # while loop to check for tags that might get empty after removing empty content tag
        while True:
            empty_tags = list_div.find_all(empty_string)
            if len(empty_tags) == 0:
                break
            [tag.decompose() for tag in empty_tags]

    if embed_css:
        for link_div in publication_soup.find_all("link", rel="stylesheet"):
            css_link = link_div["href"]
            if not css_link.endswith(".css"):
                continue
            with urllib.request.urlopen(f"{base_web_url}{css_link}") as f:
                css = f.read().decode("utf-8")
            style_tag = publication_soup.new_tag("style")
            style_tag.string = css
            list_div.insert(0, style_tag)

    html_string = str(list_div)
    return html_string
