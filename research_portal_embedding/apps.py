from django.apps import AppConfig


class ResearchPortalEmbeddingConfig(AppConfig):
    name = 'research_portal_embedding'
