from django.db import models

from .load_and_parse_html import load_and_parse_html

class Research_portal_list(models.Model):
    name = models.CharField(max_length=100, default="")
    research_portal_link = models.TextField()
    extra_html = models.TextField()
    extra_beautiful = models.JSONField()
    parsed_html = models.TextField(editable=False, default="")

    def save(self, *args, **kwargs):
        self.parsed_html = load_and_parse_html(self.research_portal_link, self.extra_html, self.extra_beautiful)
        super().save(*args, **kwargs)

    def __str__(self):
        return "{} {}".format(self.id, self.name)
