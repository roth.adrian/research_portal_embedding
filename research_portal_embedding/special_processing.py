"""
Special functions that can be used to do specific but slightly more complex stuff than the extra_beautiful functionality allows

All function have the basics outline

list_div: BeautifulSoup
    the full list to look into
domain_name: str
    the domain name for the email adresses, example "forbrf.lth.se"
**kwargs:
    depends on the function

returns: the updated list_div
"""
# import bs4
# import urllib.request
# import re

##### Written for "Research outputs" #####

# def research_outputs_search_file_links(list_div):
#     for link in list_div.find_all(name="a"):
#         with urllib.request.urlopen(link["href"]) as f:
#             html = f.read().decode("utf-8")
#         html = html.replace("\\n", "")
# 
#         sub_ps = bs4.BeautifulSoup(html, "html.parser")
#####


##### Writen for Profiles #####
"""
"""

def profiles_larger_images(publication_soup, list_div):
    """
    Changes the size of photos from default 50 px to 160 px.
    """
    img_tags = list_div.find_all(name="img", class_="image")
    if len(img_tags) == 0:
        raise ValueError("Error no image tags found")
    for img_tag in img_tags:
        img_src = img_tag["src"]
        img_src = img_src.replace("w=50", "w=160")
        img_tag["src"] = img_src
    return list_div

def profiles_add_email(publication_soup, list_div, domain_name):
    """
    Tries to add the email of each profile by using their first and last name and add the domain_name after

    domain_name: str
        the domain name for the email adresses, example "forbrf.lth.se"
    """
    name_tags = list_div.find_all(name="a", class_="person")
    if len(name_tags) == 0:
        raise ValueError("Error no name tags found")
    for name_tag in name_tags:
        all_names = name_tag.span.string.split(" ")
        chosen_names = [all_names[0], all_names[-1]]
        email = "{}@{}".format(".".join(chosen_names), domain_name).lower()
        email_div = publication_soup.new_tag(name="a", href="mailto:{}".format(email))
        email_div["class"] = ["person", "email"]
        email_div.string = email
        parent_tag = name_tag.parent.parent
        parent_tag.append(email_div)
    return list_div

#####
