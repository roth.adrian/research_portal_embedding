import setuptools

setuptools.setup(
    name="research_portal_embedding",
    version='0.0.1',
    packages=setuptools.find_packages(),
    install_requires=["bs4"],
)
