A django app for reading and caching of html lists at https://research.portal.lu.se

# Module usage
Can also be used as a python module with:
```python
python -m research_portal_embedding --research-portal-link [link to a research portal link] --output-file [html output file path]
```
or using a config file
```python
python -m research_portal_embedding --config [json config file path] --output-file [html output file path]
```

The json file can look something like
```json
{
  "research_portal_link": "https://portal.research.lu.se/en/organisations/combustion-physics/persons/?pageSize=100",

  "extra_html": "<link rel=\"stylesheet\" type=\"text/css\" href=\"https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css\">",

  "extra_beautiful": [
      [{"name": "span", "class": ["minor", "dimmed"], "string": "_re.[Dd]octoral [Ss]tudent"}, {"filter": true}],
      [{"id": "related-persons-title"}, {"set_attributes": {"string": "PhD students"} }],
      [{"class": "search-result-top"}, {"decompose": true}],
      [{"class": "hidden_search_result_heading"}, {"decompose": true}],
      [{"class": "organisations"}, {"decompose": true}],
      [{"class": "type"}, {"decompose": true}],
      [{"name": "picture"}, {"move_tag": ["img"], "set_attributes": {"alt": "", "onerror": null}}],
      [{"name": "a", "class":"person"}, {"append_html": "<i class=\"bi bi-box-arrow-up-right\">"}],
      [{"name": "script"}, {"decompose": true}]
  ]
}
```

Example configs are found in the `example_configs` folder.
For more information of the config file `extra_beautiful` or other see documentation in the source code in `load_and_parse_html.py`.
